# Install ubuntu:
1. should go to BIOS and disable secure boot first.
2. to set the matrix for the pen:
$ xinput list
This show list of devices, it will show you also the 'Stylus' and other input devices.
You should find the 'id' of Stylos (say 23) and set the matrix according to the matrix of working devices

xinput set-prop 23 --type=float "Coordinate Transformation Matrix" 0 1 0 -1 0 1 0 0 1

# install Arch:
1. disable secure boot on BIOS
2. Fn+F6 will get you to BIOS, the order of boot (usb first should change)
3. follow instructions is https://wiki.archlinux.org/title/USB_flash_installation_medium
4. Fn+F12 will give the booting media options.
5. when choosing arch linux it will show:

After following installation guide it boots into grub shell.
in grub I can see the grub.cfg file with `cat (hd0,gpt1)/grub/grub.cfg` where is shows the
UUID of the root filesystem.
I am able to boot into arch from the grub shell with:

```
> set root=(hd0,gpt1)
> linux (hd0,gpt1)/vmlinuz-linux root=UUID=bla-bla-bla
> initrd (hd0,gpt1/initramfs-linux.img
> boot
```
in the Arch shell:

to rotate the console when using outer screen:
```
echo 0 | sudo tee /sys/class/graphics/fbcon/rotate_all
```

I see /dev/mmcblk0p1 mounted on /boot
I see /dev/mmcblk0p2 mounted on /

In `/etc/fstab` I see:

```
# /dev/mmcblk1p2
UUID=69a9cb34-bfb8-495b-a227-82e12633f4a8 / ext4 rw,relatime 0 1
# /dev/mmcblk1p1
UUID=2810-905D /boot vfat rw,relatime,fmask=...
```
running `fdisk -l` shows:
```
Disk		Type
/dev/mmcblk1p1 EFI System
/dev/mmcblk1p2 Linux filesystem

I ran:
```
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
# To remove old ubuntu section from the efi
efiboomgr -b 0004 -B
grub-mkconfig -o /boot/grub/grub.cfg
```

After doing all that , still didn't work, so I enterd the install usb-stick again
and mounted the rootfs and then arch-chroot and then mounted the EFI on /boot again and
ran `grub-install` and `grub-mkconfig` again and and then make sure `efibootmgr` show what is
suppose to show and reboot and then it works.

# Configre wifi:
to see the wifi card:
lscpi -k | grep -i network
the kernel module should be loaded: `lsmod | grep wifi` shows 'iwlwifi'
then `dmesg | grep iwlwifi` to see the prints
then:
```
ip link
ip link set wlan0 up
```

in file `/etc/iwd/main.conf`:

```
[General]
EnableNetworkConfiguration=true
[Network]
NameResolvingService=systemd
```

Then:
```
sudo systemctl restart iwd
```
then set as before with the iwctl command and the commands in the `iwd` console
to enable systemd-resolved:
```
systemctl enable systemd-resolved
```

To change tty if boot fails:
ctlr+alt+F1/F2/F3

after installing budgie windows-manager according to the instructions,
I had to install a display manager. I tried lightdm but it fails, so I disabled it with:
```
systemctl disable lightdm.service
```
and installed gdm instead which works.
In addition I installed openbox which is a minimal dispaly manager that Amir said it is
good to have for backup. The to enable gdm
```
systemctl disable gdm.service
reboot
```










